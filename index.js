"use stric";

const express = require("express");
const userclaimController = require("./controllers/userclaimController"),
app = express(),
mongoose = require("mongoose"),
router = express.Router(),
testController = require("./controllers/testController");



mongoose.Promise = require('bluebird');
mongoose.connect(
    "mongodb://localhost:27017/user_claims_db"
);

const db = mongoose.connection;

db.once("open", () => {
    console.log("Connected to MongoDB");
});

app.use(express.json())
app.set("port", 8080);
app.use("/", router);

app.get("/test", testController.test);
app.get("/claims", );

app.post("/claims", userclaimController.create);

app.listen(app.get("port"), () => {
    console.log(`Server running`);
});