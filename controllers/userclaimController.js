"use strict";

const UserClaim = require("../models/UserClaims");

const MIN_DISTANCE = 200;

function compareClaims(claim1, claim2) 
{
    let x_dist = claim1.coordinates.x - claim2.coordinates.x;
    let y_dist = claim1.coordinates.y - claim2.coordinates.y;
    let distance = Math.sqrt(x_dist**2 + y_dist**2);

    if (distance < MIN_DISTANCE) {
        return false;
    }
    return true;
}
 

const getUserParams = body => {
    return {
      username: body.username,
      claimName: body.claimName,
      coordinates : {
          x : body.coordinates.x,
          y : body.coordinates.y
      }
    };
  };

module.exports = { 
    create: async (req, res, next) => {
        // console.log(req.body);
        let userClaim = new UserClaim(getUserParams(req.body));
        let userArr = await UserClaim.find({}).lean();

        for (let i = 0; i < userArr.length; i++) {
            if (!compareClaims(userArr[i], userClaim)) {
                res.json({response : "Too close"});
                return;
            }
        }
        UserClaim.create(userClaim).then( () => {
            res.json({response : "sucessfully added to database"});
            })
            .catch(error => {
            res.json({response : "something failed; RIP"})
            })
    }
}