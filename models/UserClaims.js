"use strict";

const mongoose = require ("mongoose");

const userclaimSchema = new mongoose.Schema(
    {
        username: String,
        claimName : String,
        coordinates : {
            x : Number,
            y: Number
        }
    }
);

module.exports = mongoose.model("UserClaim", userclaimSchema);